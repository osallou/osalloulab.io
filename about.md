---
layout: page
title: About me
permalink: /aboutme/
---

I am a software engineer.

I currently work at xxx on xxx.... sorry i won t say more for the moment on this ;-)


Before 2023, I worked at IRISA, an academic research institute in France in a bioinformatics core facility until 2023.

my focus was on developments and proof of concepts for grids (SGE/slurm), cloud computing (Openstack, ...), containers (Docker, Kubernete, Mesos) and around meta-data management.

* Agile development / DevOps / NoOps
* Rationalize development environment to speed up product delivery and to follow best practices.
* Development of software tools for bio-informatics research (dna/proteins analysis, dedicated query languages...)
* Setup, development and operations of Openstack cloud, open source developments for monitoring, supervision etc..
* Open source developper [https://github.com/osallou](https://github.com/osallou) and [https://gitlab.com/osallou](https://gitlab.com/osallou)
* Health and Safety Officer at IRISA for 5 years
* First aid worker

I also give some IT courses at ISTIC at Université de Rennes on linux and object oriented programming

I work mainly in python, javascript and golang, both on frontend and backend development.

I am also a Debian-Med contributor (Debian Developper)


Certifications:

* PMP certification (08/07) - Project Management Professional, certified by PMI

Hobbies:

Martial arts: vo co truyen vietnam, [http://www.longson.fr/](http://www.longson.fr/)
