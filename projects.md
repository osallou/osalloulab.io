---
layout: page
title: Projects
permalink: /projects/
---

## Projects

As an open source developper (personal and work), here are links to my main projects


* [go-docker](https://bitbucket.org/osallou/go-docker): job submission manager for docker/slurm/kubernetes/mesos for shared compute platforms (user do not need docker exec rights)
* [BioMAJ](https://github.com/genouest/biomaj) Biological data banks manager (download and transform)
* [goswift](https://github.com/osallou/goswift) Web interface for Openstack Swift
* [my account manager](https://github.com/genouest/genouestaccountmanager): tools to manage user creation/expiration, self-service etc. for our hosted services.
* [herodote](https://github.com/osallou/herodote) Serverless data manager between openstack swift and slurm or herodote-cli (remote job handler)

And many other on my gihub/gitlab account....

And a few private projects:

* gore: infrastructure as code to manage our infra and dhcp/dns/web proxy file generation from defintion and templates, in golang
* homeandco: kind of owncloud interface to access a remote file system via http and share some dir/files with temp links,  with an experimental fuse module, in golang
